// This is from Dongdong Li (qqlddg@gmail.com)

#include "mpi.h"
#include <stdio.h>
#include <assert.h>
#include <math.h>

#define FILE_TAG 1
#define RING_TAG 2
#define G 6.67384e-11L

int ringDec(int rk, int size)
{
    if (rk == 0)
        return size-1;
    else
        return --rk;
}

int ringInc(int rk, int size)
{
    if (rk == size-1)
        return 0;
    else
        return ++rk;
}
double distance(double x1, double y1, double x2, double y2)
{
    double dx = x2-x1;
    double dy = y2-y1;
    return sqrt(dx*dx+dy*dy);
}

int main( int argc, char *argv[] )
{
    int rank, size;
    int i = 0;
    int it = 0;
    MPI_Status status;
    MPI_Comm comm = MPI_COMM_WORLD;

    FILE *fp;
    int iterations;
    double x, y, m;
    double xt, yt, mt;
    double dt = 1;
    double ax = 0, ay = 0;
    double vx = 0, vy = 0;
    double r = 0;
    double dx = 0, dy = 0;
    
    MPI_Init( &argc, &argv );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    MPI_Comm_size( MPI_COMM_WORLD, &size );

    iterations = atoi(argv[1]);

    // Initial file transfer
    if (rank == 0) {
        fp = fopen(argv[2], "r");
        assert(fp);

        fscanf(fp, "%lf %lf %lf", &x, &y, &m);
        for (i = 1; i < size; ++i) {
            fscanf(fp, "%lf %lf %lf", &xt, &yt, &mt);
            MPI_Send(&xt, 1, MPI_DOUBLE, rank+1, FILE_TAG, comm);
            MPI_Send(&yt, 1, MPI_DOUBLE, rank+1, FILE_TAG, comm);
            MPI_Send(&mt, 1, MPI_DOUBLE, rank+1, FILE_TAG, comm);  
        }
        fclose(fp);
    }
    else {
        MPI_Recv(&x, 1, MPI_DOUBLE, rank-1, FILE_TAG, comm, &status);
        MPI_Recv(&y, 1, MPI_DOUBLE, rank-1, FILE_TAG, comm, &status);
        MPI_Recv(&m, 1, MPI_DOUBLE, rank-1, FILE_TAG, comm, &status);
        for (i = rank+1; i < size; ++i) {
            MPI_Recv(&xt, 1, MPI_DOUBLE, rank-1, FILE_TAG, comm, &status);
            MPI_Recv(&yt, 1, MPI_DOUBLE, rank-1, FILE_TAG, comm, &status);
            MPI_Recv(&mt, 1, MPI_DOUBLE, rank-1, FILE_TAG, comm, &status);
            
            MPI_Send(&xt, 1, MPI_DOUBLE, rank+1, FILE_TAG, comm);
            MPI_Send(&yt, 1, MPI_DOUBLE, rank+1, FILE_TAG, comm);
            MPI_Send(&mt, 1, MPI_DOUBLE, rank+1, FILE_TAG, comm);
        }
    }

    it = 0;
    
    // Begin simulation
    while (it < iterations) { 
        int step = 0;
        ax = ay = 0;
        int nextRank = rank, prevRank = rank;

        // Update status
        for (step = 0; step < size-1; ++step) {
            nextRank = ringInc(nextRank, size);
            prevRank = ringDec(prevRank, size);
            
            MPI_Send(&x, 1, MPI_DOUBLE, nextRank, RING_TAG, comm);
            MPI_Send(&y, 1, MPI_DOUBLE, nextRank, RING_TAG, comm);
            MPI_Send(&m, 1, MPI_DOUBLE, nextRank, RING_TAG, comm);

            MPI_Recv(&xt, 1, MPI_DOUBLE, prevRank, RING_TAG, comm, &status);
            MPI_Recv(&yt, 1, MPI_DOUBLE, prevRank, RING_TAG, comm, &status);
            MPI_Recv(&mt, 1, MPI_DOUBLE, prevRank, RING_TAG, comm, &status);
        

            r = distance(x, y, xt, yt);
            ax += (G * mt * (xt - x)) / (r * r * r);
            ay += (G * mt * (yt - y)) / (r * r * r);
        }

                
        dx = 0.5*ax*dt*dt+vx*dt;
        dy = 0.5*ay*dt*dt+vy*dt;

        vx += ax*dt;
        vy += ay*dt;

        x += dx;
        y += dy;
        
        ++it;
    }

    // End simulation
    if (rank == 0) {
        printf("%.9lf, %.9lf, %.2lf\n", x, y, m);
        for (i = 1; i < size; ++i) {
            MPI_Recv(&xt, 1, MPI_DOUBLE, i, RING_TAG, comm, &status);
            MPI_Recv(&yt, 1, MPI_DOUBLE, i, RING_TAG, comm, &status);
            MPI_Recv(&mt, 1, MPI_DOUBLE, i, RING_TAG, comm, &status);
            printf("%.9lf, %.9lf, %.2lf\n", xt, yt, mt);
        }
    } else {
        MPI_Send(&x, 1, MPI_DOUBLE, 0, RING_TAG, comm);
        MPI_Send(&y, 1, MPI_DOUBLE, 0, RING_TAG, comm);
        MPI_Send(&m, 1, MPI_DOUBLE, 0, RING_TAG, comm);
    }
    MPI_Finalize();
    return 0;
}
