// This is from Dongdong Li (qqlddg@gmail.com)

#include "mpi.h"
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define FILE_TAG 1
#define RING_TAG 2

#define DELTA 1e-15L

int ringDec(int rank, int size)
{
    if (rank == 0)
        return size-1;
    else
        return --rank;
}

int ringInc(int rank, int size)
{
    if (rank == size-1)
        return 0;
    else
        return ++rank;
}

int main( int argc, char *argv[] )
{
    int rank, size;
    int i = 0, j = 0, k = 0;
    int it = 0;
    MPI_Status status;
    MPI_Comm comm = MPI_COMM_WORLD;

    FILE *fp;
    
    double *cr, *ar, *br;
    int col, local_rows;
    int local_size, local_num;
    int iterations;
    
    MPI_Init( &argc, &argv );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    MPI_Comm_size( MPI_COMM_WORLD, &size );
    
    col = atoi(argv[1]);
    iterations = atoi(argv[2]);
    int line_size = col * sizeof(double);
    local_rows = col / size;
    local_size = line_size * local_rows;
    local_num = col * local_rows;
    
    cr = malloc(local_size);
    ar = malloc(local_size);
    br = malloc(local_size);
    memset(cr, 0, local_size);
    
    // Initial file transfer
    if (rank == 0) {
        fp = fopen(argv[3], "r");
        assert(fp);
        for (i = 0; i < local_rows; ++i)
            for (j = 0; j < col; ++j) {
                int index = i*col+j;
                fscanf(fp, "%lf", &ar[index]);
            }
        
        for (k = 1; k < size; ++k) {
            for (i = 0; i < local_rows; ++i)
                for (j = 0; j < col; ++j) {
                    int index = i*col+j;
                    fscanf(fp, "%lf", &br[index]);
                }
            MPI_Send(br, local_num, MPI_DOUBLE, rank+1, FILE_TAG, comm);
        }
        fclose(fp);
    }    
    else {
        MPI_Recv(ar, local_size, MPI_DOUBLE, rank-1, FILE_TAG, comm, &status);
    
        for (i = rank+1; i < size; ++i) {
            MPI_Recv(br, local_num, MPI_DOUBLE, rank-1, FILE_TAG, comm, &status);
            MPI_Send(br, local_num, MPI_DOUBLE, rank+1, FILE_TAG, comm);
        }
    }
    
    // Begin caculation
    it = 0;
    while (it < iterations) { 
        int nextRank = rank, prevRank = rank;
        
        
        // Local caculation
        for (i = 0; i < local_rows; ++i) {
            for (j  = 0; j < local_rows; ++j) {
                for (k = 0; k < col; ++k) {
                    int indexC = i*col+rank*local_rows+j;
                    int indexA = i*col+k;
                    int indexB = j*col+k;
                    cr[indexC] += ar[indexA] * ar[indexB];
                }
            }
        }
    
        // External(global) caculation
        int step;
        for (step = 0; step < size-1; ++step) {
            nextRank = ringInc(nextRank, size);
            prevRank = ringDec(prevRank, size);

            MPI_Send(ar, local_num, MPI_DOUBLE, nextRank, RING_TAG, comm);

            MPI_Recv(br, local_num, MPI_DOUBLE, prevRank, RING_TAG, comm, &status);

            for (i = 0; i < local_rows; ++i) {
                for (j  = 0; j < local_rows; ++j) {
                    for (k = 0; k < col; ++k) {
                        int indexC = i*col+prevRank*local_rows+j;
                        int indexA = i*col+k;
                        int indexB = j*col+k;
                        cr[indexC] += ar[indexA] * br[indexB];
                    }
                }
            }
        }
        
        ++it;
    }
    
    if (rank == 0) {
        for (i = 0; i < local_rows; ++i) {
            for (j = 0; j < col; ++j) {
                int index = i*col+j;
                printf("%.2lf ", cr[index]);
            }
            printf("\n");
        }
        
        
        for (k = 1; k < size; ++k) {
            MPI_Recv(cr, local_num, MPI_DOUBLE, k, RING_TAG, comm, &status);
            
            for (i = 0; i < local_rows; ++i) {
                for (j = 0; j < col; ++j) {
                    int index = i*col+j;
                    printf("%.2lf ", cr[index]);
                }
                printf("\n");
            }
        }
    } else {
        MPI_Send(cr, local_num, MPI_DOUBLE, 0, RING_TAG, comm);
    }
    
    free(cr);
    free(br);
    free(ar);
    MPI_Finalize();
    return 0;
}
