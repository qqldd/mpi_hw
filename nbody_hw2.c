// This is from Dongdong Li (qqlddg@gmail.com)

#include "mpi.h"
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define FILE_TAG 1
#define RING_TAG 2
#define G 6.67384e-11L
#define DELTA 1e-15L

int ringDec(int rank, int size)
{
    if (rank == 0)
        return size-1;
    else
        return --rank;
}

int ringInc(int rank, int size)
{
    if (rank == size-1)
        return 0;
    else
        return ++rank;
}
double distance(double x1, double y1, double x2, double y2)
{
    double dx = x2-x1;
    double dy = y2-y1;
    return sqrt(dx*dx+dy*dy);
}
int main( int argc, char *argv[] )
{
    int rank, size;
    int i = 0, j = 0;
    int it = 0;
    MPI_Status status;
    MPI_Comm comm = MPI_COMM_WORLD;

    FILE *fp;
    int iterations;
    int granularity = 0;
    double *x, *y, *m;
    double *xt, *yt, *mt;
    double dt = 1;
    double *ax, *ay;
    double *vx, *vy;
    double r = 0;
    double *dx, *dy;
    
    MPI_Init( &argc, &argv );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    MPI_Comm_size( MPI_COMM_WORLD, &size );
    
    double t_start = MPI_Wtime();
    
    iterations = atoi(argv[1]);
    granularity = atoi(argv[2]);

    assert(granularity);
    int local_size = sizeof(double) * granularity;
    x = malloc(local_size);
    y = malloc(local_size);
    m = malloc(local_size);
    xt = malloc(local_size);
    yt = malloc(local_size);
    mt = malloc(local_size);

    dx = malloc(local_size);
    dy = malloc(local_size);
    vx = malloc(local_size);
    vy = malloc(local_size);
    ax = malloc(local_size);
    ay = malloc(local_size);
    
    memset(vx, 0, local_size);
    memset(vy, 0, local_size);
    

    // Initial file transfer
    if (rank == 0) {
        fp = fopen(argv[3], "r");
        assert(fp);
        for (i = 0; i < granularity; ++i) {
            fscanf(fp, "%lf %lf %lf", &x[i], &y[i], &m[i]);
        }
        for (i = 1; i < size; ++i) {
            for (j = 0; j < granularity; ++j) {
                fscanf(fp, "%lf %lf %lf", &xt[j], &yt[j], &mt[j]);
            }
            MPI_Send(xt, granularity, MPI_DOUBLE, rank+1, FILE_TAG, comm);
            MPI_Send(yt, granularity, MPI_DOUBLE, rank+1, FILE_TAG, comm);
            MPI_Send(mt, granularity, MPI_DOUBLE, rank+1, FILE_TAG, comm);  
        }
        fclose(fp);
    }
    else {
        MPI_Recv(x, granularity, MPI_DOUBLE, rank-1, FILE_TAG, comm, &status);
        MPI_Recv(y, granularity, MPI_DOUBLE, rank-1, FILE_TAG, comm, &status);
        MPI_Recv(m, granularity, MPI_DOUBLE, rank-1, FILE_TAG, comm, &status);

        for (i = rank+1; i < size; ++i) {
            MPI_Recv(xt, granularity, MPI_DOUBLE, rank-1, FILE_TAG, comm, &status);
            MPI_Recv(yt, granularity, MPI_DOUBLE, rank-1, FILE_TAG, comm, &status);
            MPI_Recv(mt, granularity, MPI_DOUBLE, rank-1, FILE_TAG, comm, &status);
            
            MPI_Send(xt, granularity, MPI_DOUBLE, rank+1, FILE_TAG, comm);
            MPI_Send(yt, granularity, MPI_DOUBLE, rank+1, FILE_TAG, comm);
            MPI_Send(mt, granularity, MPI_DOUBLE, rank+1, FILE_TAG, comm);
        }
    }
    
    // Begin simulation
    it = 0;
    while (it < iterations) { 
        int step = 0;
        int granu_i = 0;
        int nextRank = rank, prevRank = rank;
        memset(ax, 0, local_size);
        memset(ay, 0, local_size);
        
        // Update status
        // Local accelerate
        for (i = 0; i < granularity; ++i) {
            for (j  = 0; j < granularity; ++j) {
                r = distance(x[i], y[i], x[j], y[j]);
                if (r == 0) continue;
                ax[i] += (G * m[j] * (x[j] - x[i])) / (r * r * r);
                ay[i] += (G * m[j] * (y[j] - y[i])) / (r * r * r);
            }
        }
        
        // External(global) accelerate
        for (step = 0; step < size-1; ++step) {
            nextRank = ringInc(nextRank, size);
            prevRank = ringDec(prevRank, size);

            MPI_Send(x, granularity, MPI_DOUBLE, nextRank, RING_TAG, comm);
            MPI_Send(y, granularity, MPI_DOUBLE, nextRank, RING_TAG, comm);
            MPI_Send(m, granularity, MPI_DOUBLE, nextRank, RING_TAG, comm);

            MPI_Recv(xt, granularity, MPI_DOUBLE, prevRank, RING_TAG, comm, &status);
            MPI_Recv(yt, granularity, MPI_DOUBLE, prevRank, RING_TAG, comm, &status);
            MPI_Recv(mt, granularity, MPI_DOUBLE, prevRank, RING_TAG, comm, &status);

        
            for (i = 0; i < granularity; ++i) {
                for (j  = 0; j < granularity; ++j) {
                    r = distance(x[i], y[i], xt[j], yt[j]);
                    if (r == 0) continue;
                    ax[i] += (G * mt[j] * (xt[j] - x[i])) / (r * r * r);
                    ay[i] += (G * mt[j] * (yt[j] - y[i])) / (r * r * r);
                }
            }
        }
        
        for (i = 0; i < granularity; ++i) {
            dx[i] = 0.5*ax[i]*dt*dt+vx[i]*dt;
            dy[i] = 0.5*ay[i]*dt*dt+vy[i]*dt;

            vx[i] += ax[i]*dt;
            vy[i] += ay[i]*dt;            

            x[i] += dx[i];
            y[i] += dy[i];
        }
        
        ++it;
    }
    
    // End simulation
    if (rank == 0) {
        for (i = 0; i < granularity; ++i)
            printf("%.9lf, %.9lf, %.2lf\n", x[i], y[i], m[i]);
        for (i = 1; i < size; ++i) {
            MPI_Recv(xt, granularity, MPI_DOUBLE, i, RING_TAG, comm, &status);
            MPI_Recv(yt, granularity, MPI_DOUBLE, i, RING_TAG, comm, &status);
            MPI_Recv(mt, granularity, MPI_DOUBLE, i, RING_TAG, comm, &status);
            for (j = 0; j < granularity; ++j)
                printf("%.9lf, %.9lf, %.2lf\n", xt[j], yt[j], mt[j]);
        }
    } else {
        MPI_Send(x, granularity, MPI_DOUBLE, 0, RING_TAG, comm);
        MPI_Send(y, granularity, MPI_DOUBLE, 0, RING_TAG, comm);
        MPI_Send(m, granularity, MPI_DOUBLE, 0, RING_TAG, comm);
    }
    
    free(x);
    free(y);
    free(m);
    free(xt);
    free(yt);
    free(mt);
    free(ax);
    free(ay);
    free(dx);
    free(dy);
    
    double t_elapsed = MPI_Wtime() - t_start;
    double t_tmp;
    if (rank == 0) {
        for (i = 1; i < size; ++i) {
            MPI_Recv(&t_tmp, 1, MPI_DOUBLE, i, RING_TAG, comm, &status);
            if (t_tmp > t_elapsed)
                t_elapsed = t_tmp;
        }
        printf("\nMax time elapsed %lf\n", t_elapsed);
    } else {
        MPI_Send(&t_elapsed, 1, MPI_DOUBLE, 0, RING_TAG, comm);
    }
    MPI_Finalize();
    

    return 0;
}
