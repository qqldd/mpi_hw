# This is from Dongdong Li (qqlddg@gmail.com)

FILES = nbody_hw1 nbody_hw2 matrix_hw2 nbody_hw3 mpe_hw4 nbody_hw4 man_hw7 cli_hw7

all:
	#$(foreach SF, $(FILES), mpicc $(SF).c -lm -o $(SF);)
	make hw1
	make hw2
	make hw3
	make hw4
	make hw7
	
hw1:
	mpicc nbody_hw1.c -lm -o nbody_hw1
	
hw2:
	mpicc nbody_hw2.c -lm -o nbody_hw2
	mpicc matrix_hw2.c -o matrix_hw2
	
hw3:
	mpicc nbody_hw3.c -lm -o nbody_hw3
	
hw4:
	mpicc mpe_hw4.c -lm -lmpe -lX11 -o mpe_hw4
	mpicc nbody_hw4.c -lm -llmpe -lmpe -o nbody_hw4
	
hw7:
	mpicc man_hw7.c -o man_hw7
	mpicc cli_hw7.c -o cli_hw7
	
clean:
	rm $(FILES)