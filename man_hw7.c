/* manager */ 
#include "mpi.h" 
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int main( int argc, char *argv[] )
{
    int rank;
    int size;
    int errcodes[30];
    int cli_num;
    MPI_Comm parentcomm, intercomm;
    char greeting[64];
    int i;

    cli_num = atoi(argv[1]);
    assert(argc > 1);
    MPI_Init( &argc, &argv );
    MPI_Status stat;
    MPI_Comm_get_parent( &parentcomm );
    if (parentcomm == MPI_COMM_NULL)
    {
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Comm_size(MPI_COMM_WORLD, &size);

        MPI_Comm_spawn( "./cli_hw7", MPI_ARGV_NULL, cli_num, MPI_INFO_NULL, 0, MPI_COMM_WORLD, &intercomm, errcodes );
        
        sprintf(greeting, "Greeting from master rank %d of %d", rank, size);
        for(i = 0; i+rank < cli_num; i+=size)
            MPI_Send(greeting, 64, MPI_BYTE, i+rank,1,intercomm);
    }

    fflush(stdout);
    MPI_Finalize();
    return 0;
}
