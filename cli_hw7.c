#include "mpi.h"
#include <stdio.h>

int main(int argc, char *argv[])
{
    int rank;
    int size, parentsize;
    int errcodes[30];
    MPI_Comm parentcomm, intercomm;
    char greeting[64];
    int i;
    
    MPI_Init( &argc, &argv );
    MPI_Status stat;
    MPI_Comm_get_parent( &parentcomm );
    if (parentcomm == MPI_COMM_NULL)
    {
        printf("Error!\n");
        return -1;
    }
    else
    {
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        MPI_Comm_remote_size(parentcomm, &parentsize); 
        MPI_Recv(greeting, 64, MPI_BYTE, rank%parentsize, 1, parentcomm, &stat);
        printf("Slave %d Receive: %s\n", rank, greeting);

    }
    fflush(stdout);
    MPI_Finalize();
    return 0;
}

